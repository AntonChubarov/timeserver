FROM golang:1.17.5-alpine3.15 AS builder

WORKDIR /application/

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build main.go


FROM alpine:latest

WORKDIR /application/

COPY --from=builder /application ./

RUN apk add --no-cache tzdata

ENTRYPOINT [ "./main" ]

# sudo docker build -f Dockerfile -t antonchubarov/timeserver:0.0.1 .
# sudo docker images
# sudo docker run -d -p 8666:8666 <IMAGE_ID>
# sudo docker ps