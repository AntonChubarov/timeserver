package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"time"
)

type timeDTO struct {
	Unix     int64
	Year     int
	Month    string
	Day      int
	Hour     int
	Minute   int
	Second   int
	TimeZone string
}

func main() {
	e := echo.New()

	e.Use(middleware.Logger())

	e.GET("/time", getTime)

	e.Logger.Fatal(e.Start(":8666"))
}

func getTime(c echo.Context) error {
	locationName := c.QueryParam("location")
	if locationName == "" {
		locationName = "GMT"
	}

	t, err := getTimeDTO(locationName)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, t)
}

func getTimeDTO(locationName string) (*timeDTO, error) {
	tn := time.Now()

	loc, err := time.LoadLocation(locationName)
	if err != nil {
		return nil, err
	}

	tn = tn.In(loc)

	_, _, day := tn.Date()
	zone, _ := tn.Zone()

	t := &timeDTO{
		Unix:     tn.Unix(),
		Year:     tn.Year(),
		Month:    tn.Month().String(),
		Day:      day,
		Hour:     tn.Hour(),
		Minute:   tn.Minute(),
		Second:   tn.Second(),
		TimeZone: zone,
	}

	return t, nil
}
