package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetTimeDTO(t *testing.T) {
	zones := []string{
		"Europe/London",
		"Europe/Kiev",
		"Asia/Tokyo",
	}

	for i := 0; i < len(zones); i++ {
		tau, err := getTimeDTO(zones[i])
		assert.NotNil(t, tau)
		assert.NoError(t, err)
	}
}
